using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XMLObjects;

namespace XPF
{
	public class Manager<T>
	{
		public delegate T ObjectCreator(XMLNode node, Managers m);
		public delegate T UnresolvedCreator(string id);

		protected UnresolvedCreator unresolvedCreator;

		protected Dictionary<string, ObjectCreator> creators = new Dictionary<string, ObjectCreator>();
		protected Dictionary<string, T> objects = new Dictionary<string, T>();

		public Managers Managers
		{
			get;
			private set;
		}

		public Manager(Managers m)
		{
			this.Managers = m;
		}

		public ObjectCreator this[string key]
		{
			get
			{
				return this.objects.ContainsKey(key) ? this.creators[key] : default(ObjectCreator);
			}
			set
			{
				if (this.creators.ContainsKey(key))
				{
					Console.WriteLine($"Warning: Key '{key}' is overridden in Manager for {typeof(T).Name}");
					this.creators[key] = value;
				}
				else
					this.creators.Add(key, value);
			}
		}

		public void Register(string type, ObjectCreator creator)
		{
			AddCreator(type, creator);
		}

		public virtual void AddCreator(string type, ObjectCreator creator)
		{
			this[type] = creator;
		}

		public ObjectCreator GetCreator(string type)
		{
			return this[type];
		}


		public T GetObject(XMLNode node, bool suppressError = false)
		{
			string src = null;
			T obj;

			if (node.Attributes.ContainsKey("src"))
			{
				src = node.Attributes["src"];
				if (this.objects.ContainsKey(src))
					return this.objects[src];
				else if (this.creators.ContainsKey(node.Name))
					return this.unresolvedCreator(src);
			}
			if(this.creators.ContainsKey(node.Name))
			{
				obj = this.creators[node.Name](node, this.Managers);
				if(node.HasAttr("id"))
					this.objects[node["id"]] = obj;
				return obj;
			}
			else if(suppressError)
				return default(T);
			else
				throw new Exception($"Unknown type '{node.Name}'");
        }

		public T ResolveObject(Unresolved<T> unresolved)
		{
			if(!this.objects.ContainsKey(unresolved.id))
				throw new Exception("ID '{unresolved.id}' couldn't be resolved");
			return this.objects[unresolved.id];
		}
	}

	public class CreatorNotFoundException : Exception
	{
		public CreatorNotFoundException(string name) : base($"No creator for '{name}' found")
		{ }
	}
}
