using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XMLObjects;

namespace XPF
{
	public static class StoredValues
	{
		public static Dictionary<string, string> values = new Dictionary<string, string>();

		public static bool SetVar(string key, string val)
		{
			if(values.ContainsKey(key))
			{
				values[key] = val;
				return false;
			}
			values.Add(key, val);
			return true;
		}

		public static string GetVar(string key)
		{
			return values.ContainsKey(key) ? values[key] : null;
		}

		public static bool HasVar(string key)
		{
			return values.ContainsKey(key);
		}
	}

	public class SetVar : ConditionBase, IAction
	{
		public string key;
		public string val;

		public SetVar(XMLNode node) : base(node)
		{
			if(!node.HasAttr("key"))
				throw new Exception("SETVAR must have a key-attribute");
			if(node.HasValue)
				this.val = node.Value;
		}

		public void Run(File f)
		{
			StoredValues.SetVar(this.key, this.val);
		}

		protected override bool InternalTest(File f)
		{
			return StoredValues.SetVar(this.key, this.val);
		}

		public override object GetReturnValue()
		{
			return this.val;
		}
	}

	public class GetVar : ConditionBase, IAction
	{
		public string key;

		public GetVar(XMLNode node)
		{
			if(!node.HasAttr("key"))
				throw new Exception("GETVAR must have a key-attribute");
			if(node.HasSubNodes || node.HasValue)
				throw new Exception("GETVAR mustn't have a value or subnodes");
			this.key = node["key"];
		}

		protected override bool InternalTest(File f)
		{
			return StoredValues.HasVar(this.key);
		}

		public override object GetReturnValue()
		{
			return StoredValues.GetVar(this.key);
		}

		public void Run(File f)
		{ }
	}
}
