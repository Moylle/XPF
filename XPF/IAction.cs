﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XMLObjects;

namespace XPF
{
	public interface IAction
	{
		void Run(File f);
	}

	public abstract class ActionBase : IAction
	{
		public readonly string id;

		public ActionBase(string id = null)
		{
			this.id = id;
		}

		public ActionBase(XMLNode node)
		{
			if (node == null)
				throw new ArgumentNullException(nameof(node));
			if (node.HasAttr("id"))
				this.id = node.Attributes["id"];
		}

		public abstract void Run(File f);
	}
}
