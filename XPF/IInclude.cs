using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XMLObjects;

namespace XPF
{
	public interface IInclude
	{
		void Include();
	}

	public class Include : IInclude
	{
		public List<IInclude> includes = new List<IInclude>();

		public Include(XMLNode node, Managers m)
		{
			if (node.HasValue)
				throw new Exception("INCLUDE mustn't have a value");
			foreach (XMLNode subNode in node.SubNodes)
				this.includes.Add(m.im.GetObject(subNode));
		}

		void IInclude.Include()
		{
			foreach (IInclude i in this.includes)
				i.Include();
		}

		[Register]
		public static void Register(Managers m)
		{
			m.im.Register("include", (node, managers) => new Include(node, managers));
		}
	}
}
