using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using XMLObjects;

namespace XPF
{
	public class DynamicStringManager : Manager<IDynamicString>
	{
		private bool hooked;
		private ISet<Thread> threads = new HashSet<Thread>();

		public DynamicStringManager(Managers m) : base(m)
		{
			DynamicStringManager dm = this;
			this.unresolvedCreator = (id) => new UnresolvedDynamicString(id, dm);
		}

		public override void AddCreator(string type, ObjectCreator creator)
		{
			base.AddCreator(type, creator);
			if(!this.hooked)
			{
				RegisterHooks();
				this.hooked = true;
			}
		}

		void RegisterHooks()
		{
			XMLNode.HasValueAccess += HasValueHook;
			XMLNode.SubNodesAccess += SubNodesHook;
			XMLNode.ValueAccess += ValueHook;
		}


		public void HasValueHook(EventArgs<bool> e)
		{
			if(!HasDynamicString(e.node))
				return;
			e.value = true;
		}
		public void SubNodesHook(EventArgs<List<XMLNode>> e)
		{
			if(e.accessType == AccessType.SET || e.value.Count != 1 || !this.creators.ContainsKey(e.value[0].Name))
				return;
			if(this.threads.Contains(Thread.CurrentThread))
				return;
			e.value = new List<XMLNode>();
		}
		public void ValueHook(EventArgs<string> e)
		{
			XMLNode dynamicString = GetDynamicString(e.node);
			if(dynamicString == null)
				return;
			if(e.accessType == AccessType.SET)
				e.canceled = true;
			else
				e.value = GetObject(dynamicString).GetString();
		}

		private bool HasDynamicString(XMLNode n)
		{
			return GetDynamicString(n) != null;
		}
		private XMLNode GetDynamicString(XMLNode n)
		{
			try
			{
				this.threads.Add(Thread.CurrentThread);
				List<XMLNode> subNodes = n.SubNodes;
				return subNodes != null && subNodes.Count == 1 && this.creators.ContainsKey(subNodes[0].Name) ? subNodes[0] : null;
			}
			finally
			{
				this.threads?.Remove(Thread.CurrentThread);
			}
		}
	}
}
