using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XMLObjects;

namespace XPF
{
	public class XMLManager
	{
		private string srcDir;
		private string dstDir;

		public string SrcDir
		{
			get
			{
				return this.srcDir;
			}
			set
			{
				if (!Directory.Exists(value))
					throw new Exception($"Source directory '{value}' doesn't exist");
				this.srcDir = value;
				File.RootPath = value;
			}
		}

		public string DstDir
		{
			get
			{
				return this.dstDir;
			}

			set
			{
				if(!Directory.Exists(value))
					throw new Exception($"Destination directory '{value}' doesn't exist");
				this.dstDir = value;
				File.DstRootPath = value;
			}
		}

		public File CurrentFile
		{
			get;
			protected set;
		}


		public XMLNode rootNode;
		Managers m;

		List<IInclude> includes;
		List<IAction> actions;
		List<ICondition> conditions;
		List<IOn> ons;
		List<ISetting> settings;

		public XMLManager(Managers m)
		{
			this.m = m;
		}

		public void Run()
		{
			ResolveXML();
			Dictionary<int, List<IOn>> ons = SortOns(this.ons);
			File[] files = GetFiles(this.srcDir);

			foreach(KeyValuePair<int, List<IOn>> pOns in ons)
			{
				Console.WriteLine($"Info: Processing priority {pOns.Key}");
				foreach (File f in files)
				{
					Console.WriteLine($"Info: Processing file '{f.RelativePath}'");
					this.CurrentFile = f;
					foreach(IOn on in pOns.Value)
						on.Run(f);
				}
			}
			Console.WriteLine("Info: All files processed");
        }

		File[] GetFiles(string path)
		{
			List<File> files = new List<File>();
			File f;
			foreach (string s in Directory.EnumerateFileSystemEntries(path, "*", SearchOption.AllDirectories))
			{
				files.Add(f = new File(s, Directory.Exists(s)));
				Console.WriteLine($"Info: Discovered file {f.RelativePath}");
			}
			return files.ToArray();
		}

		void ResolveXML()
		{
			IInclude include;
			IAction action;
			ICondition condition;
			IOn on;
			ISetting setting;

			this.includes = new List<IInclude>();
			this.actions = new List<IAction>();
			this.conditions = new List<ICondition>();
			this.ons = new List<IOn>();
			this.settings = new List<ISetting>();

			for (int i = 0; i < this.rootNode.SubNodes.Count; ++i)
				if ((include = this.m.im.GetObject(this.rootNode.SubNodes[i], true)) != null)
				{
					this.includes.Add(include);
					include.Include();
				}
			foreach (XMLNode node in this.rootNode.SubNodes)
				if ((setting = this.m.sm.GetObject(node, true)) != null)
				{
					this.settings.Add(setting);
					setting.Apply();
				}
			
			if (this.srcDir == null)
				throw new Exception("No source directory defined");
			if (this.dstDir == null)
				throw new Exception("No destination directory defined");

			foreach (XMLNode node in this.rootNode.SubNodes)
				if ((condition = this.m.cm.GetObject(node, true)) != null)
					this.conditions.Add(condition);
			foreach (XMLNode node in this.rootNode.SubNodes)
				if ((action = this.m.am.GetObject(node, true)) != null)
					this.actions.Add(action);
			foreach (XMLNode node in this.rootNode.SubNodes)
				if ((on = this.m.om.GetObject(node, true)) != null)
					this.ons.Add(on);
		}

		Dictionary<int, List<IOn>> SortOns(List<IOn> ons)
		{
			ons.OrderBy((o) => o.GetPriority());
			Dictionary<int, List<IOn>> d = new Dictionary<int, List<IOn>>();
			int priority;
			foreach (IOn on in ons)
				if (d.ContainsKey(priority = on.GetPriority()))
					d[priority].Add(on);
				else
					d.Add(priority, new List<IOn>() { on });
			return d;
		}
	}
}
