using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Xml;
using XMLObjects;

namespace XPF
{
	class Program
	{
		private Managers m;
		public Managers Managers
		{
			get
			{
				return m ?? (m = Managers.Init());
			}
		}

		public static void Main(string[] args)
		{
			new Program().Run(args);
			Console.WriteLine("Done");
#if debug
			Console.ReadLine();
#endif
		}

		void Run(string[] args)
		{
			Logger.Init();
#if !debug
			try
			{
#endif
				ProcessArgs(ref args);
#if !debug
			}
			catch(Exception x)
			{
				Console.WriteLine($"Error: Error while processing command line arguments");
				Console.WriteLine(x.ToString());
			}
#endif

			XMLModifications.Apply();

			XMLNode rootNode;
			try
			{
				rootNode = XMLNode.Create(new XmlTextReader(args[0]));
			}
			catch(Exception x)
			{
				Console.WriteLine("Error: Error while reading configuration file");
				Console.WriteLine(x.ToString());
				return;
			}

			Managers m = this.Managers;
#if !debug
			try
			{
#endif
				m.xm.rootNode = rootNode;
				m.xm.Run();
#if !debug
			}
			catch(Exception x)
			{
				Console.WriteLine("Error: Error while executing");
				Console.WriteLine(x.ToString());
				Environment.ExitCode = 1;
			}
#endif
		}

		void ProcessArgs(ref string[] args)
		{
			Queue<string> q = new Queue<string>(args);
			string currentArg;
			while(q.Count > 1)
			{
				switch(currentArg = q.Dequeue())
				{
					case "-base":
					case "--extract-base":
						Base.Extract();
						Environment.Exit(0);
						return;
					case "-q":
					case "--quiet":
						Logger.consoleLogLevel = Logger.LogLevel.NONE;
						break;
					case "-ll":
					case "--log-level":
					case "-fll":
					case "--file-log-level":
					case "-cll":
					case "--console-log-level":
						if(q.Count == 0)
							throw new Exception($"Missing argument after {currentArg}");
						Logger.LogLevel ll;
						if(!Enum.TryParse(q.Dequeue(), out ll))
							throw new Exception($"Unknown argument after {currentArg}, possible values: INFO, WARNING, ERROR, NONE");
						switch(currentArg)
						{
							case "-ll":
							case "--log-level":
								Logger.fileLogLevel = ll;
								Logger.consoleLogLevel = ll;
								break;
							case "-fll":
							case "--file-log-level":
								Logger.fileLogLevel = ll;
								break;
							case "-cll":
							case "--console-log-level":
								Logger.consoleLogLevel = ll;
								break;
						}
						break;
					case "-l":
					case "--log":
						if(q.Count == 0)
							throw new Exception($"Missing argument after {currentArg}");
						Logger.SetLogFile(q.Dequeue());
						break;
					default:
						throw new Exception($"Unknown argument '{currentArg}'");
				}
			}
			if(q.Count != 1)
				throw new Exception("Usage: [options] <configuration file> | --extract-base");
			args = q.ToArray();
			if(!System.IO.File.Exists(args[0]))
				throw new Exception($"Input file '{args[0]}' not found");
		}
	}
}
