using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XMLObjects;

namespace XPF
{

	public abstract class BaseCondition : ConditionBase
	{
		public List<ICondition> conditions = new List<ICondition>();
		
		public BaseCondition(string id = null, string[] classes = null, bool? forcedState = null) : base(id, classes, forcedState)
		{ }
		public BaseCondition(XMLNode node, Managers m) : base(node)
		{
			if (node.HasValue)
				throw new Exception(GetType().Name + " is a base condition and mustn't have text");
			foreach (XMLNode subNode in node.SubNodes)
				this.conditions.Add(m.cm.GetObject(subNode));
		}
	}

	public abstract class StandaloneCondition : ConditionBase
	{

		public StandaloneCondition(string id = null, string[] classes = null, bool? forcedState = null) : base(id, classes, forcedState)
		{ }
		public StandaloneCondition(XMLNode node) : base(node)
		{
			if (node.HasValue)
				throw new Exception(GetType().Name + " is a standalone condition and mustn't have text");
			if (node.HasSubNodes)
				throw new Exception(GetType().Name + " is a standalone condition and mustn't have sub nodes");
		}
	}

	public class AndCondition : BaseCondition
	{

		public AndCondition(string id = null, string[] classes = null, bool? forcedState = null) : base(id, classes, forcedState)
		{ }
		public AndCondition(XMLNode node, Managers m) : base(node, m)
		{ }

		protected override bool InternalTest(File f)
		{
			foreach (ICondition cond in this.conditions)
				if (!cond.Test(f))
					return false;
			return true;
		}

		[Register]
		public static void Register(Managers m)
		{
			ConditionManager.ObjectCreator oc = (node, managers) => new AndCondition(node, managers);
			m.cm.Register("and", oc);
			m.cm.Register("cond", oc);
		}
	}

	public class OrCondition : BaseCondition
	{
		public OrCondition(string id = null, string[] classes = null, bool? forcedState = null) : base(id, classes, forcedState)
		{ }
		public OrCondition(XMLNode node, Managers m) : base(node, m)
		{ }

		protected override bool InternalTest(File f)
		{
			foreach (ICondition cond in this.conditions)
				if (cond.Test(f))
					return true;
			return false;
		}

		[Register]
		public static void Register(Managers m)
		{
			m.cm["or"] = (node, manager) => new OrCondition(node, manager);
		}
	}

	public class XorCondition : BaseCondition
	{
		public XorCondition(string id = null, string[] classes = null, bool? forcedState = null) : base(id, classes, forcedState)
		{ }
		public XorCondition(XMLNode node, Managers m) : base(node, m)
		{ }

		protected override bool InternalTest(File f)
		{
			bool b = false;
			foreach (ICondition cond in this.conditions)
				b ^= cond.Test(f);
			return b;
		}

		[Register]
		public static void Register(Managers m)
		{
			m.cm["xor"] = (node, managers) => new XorCondition(node, managers);
		}
	}

	public class NotCondition : ConditionBase
	{
		public ICondition condition;

		public NotCondition(string id = null, string[] classes = null, bool? forcedState = null) : base(id, classes, forcedState)
		{ }
		public NotCondition(XMLNode node, Managers m) : base(node)
		{
			this.condition = new AndCondition(node, m);
		}

		protected override bool InternalTest(File f)
		{
			return !this.condition.Test(f);
		}

		[Register]
		public static void Register(Managers m)
		{
			m.cm["not"] = (node, managers) => new NotCondition(node, managers);
		}
	}

	public class BoolCondition : StandaloneCondition
	{
		public bool state;

		public BoolCondition(string id = null, string[] classes = null, bool? forcedState = null) : base(id, classes, forcedState)
		{ }
		public BoolCondition(XMLNode node) : base(node)
		{
			if (node.Name == "true")
				this.state = true;
			else
				this.state = false;
		}

		protected override bool InternalTest(File f)
		{
			return this.state;
		}

		[Register]
		public static void Register(Managers m)
		{
			m.cm.Register("false", (node, managers) => new BoolCondition(node));
			m.cm.Register("true", (node, managers) => new BoolCondition(node));
		}
	}
}
