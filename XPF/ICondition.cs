﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XMLObjects;

namespace XPF
{
	public interface ICondition
	{
		bool Test(File f);

		object GetReturnValue();
	}

	public abstract class ConditionBase : ICondition
	{
		public readonly string id;
		public readonly string[] classes;
		public readonly bool? forcedState;


		public ConditionBase(string id = null, string[] classes = null, bool? forcedState = null)
		{
			this.id = id;
			this.classes = classes;
			this.forcedState = forcedState;
		}

		public ConditionBase(XMLNode node)
		{
			if (node == null)
				throw new ArgumentNullException(nameof(node));
			if (node.HasAttr("id"))
				this.id = node.Attributes["id"];
			if (node.HasAttr("state"))
			{
				string state = node.Attributes["state"].ToLower();
				bool b;
				if (!bool.TryParse(state, out b))
                {
					Console.WriteLine($"Warning: Invalid value in attribute state -> '{state}'. Must be TRUE or FALSE");
					this.forcedState = null;
				}
				else
					this.forcedState = b;
			}
			if (node.HasAttr("class"))
				this.classes = node.Attributes["class"].Split(' ');
		}

		public virtual bool Test(File f)
		{
			bool b = InternalTest(f);
			return this.forcedState ?? b;
		}

		public virtual object GetReturnValue()
		{
			return null;
		}

		protected abstract bool InternalTest(File f);
	}
}
