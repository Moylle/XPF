using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XMLObjects;

namespace XPF
{
	public interface IOn
	{
		void Run(File f);

		int GetPriority();
	}

	public class On : IOn
	{
		public int priority = 0;
		public ICondition cond;
		public IAction action;

		public On(XMLNode node, Managers m)
		{
			if (node.HasValue)
				throw new Exception("ON mustn't have a value");
			if (node.HasAttr("priority"))
				this.priority = Convert.ToInt32(node["priority"]);
			XMLNode conditionNode = new XMLNode();
			foreach(XMLNode subNode in node.SubNodes)
				if (this.action != null)
					throw new Exception("After a THEN no further sub nodes are allowed");
				else if (subNode.Name == "then")
					this.action = new Action(subNode, m);
				else
					conditionNode.SubNodes.Add(subNode);
			this.cond = new AndCondition(conditionNode, m);
		}

		public void Run(File f)
		{
			if (this.cond.Test(f))
				this.action.Run(f);
		}

		public int GetPriority()
		{
			return this.priority;
		}

		[Register]
		public static void Register(Managers m)
		{
			m.om.Register("on", (node, managers) => new On(node, managers));
		}
	}
}
