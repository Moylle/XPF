using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace XPF
{
	public static class Logger
	{
		public enum LogLevel
		{
			NONE = 0,
			ERROR = 1,
			WARNING = 2,
			INFO = 3
		}

		private static TextWriter defOut;
		private static bool init = false;
		private static Stream outputStream;
		private static Log log;

		public static LogLevel consoleLogLevel = LogLevel.INFO;
		public static LogLevel fileLogLevel = LogLevel.INFO;

		static Logger()
		{
			Init();
		}

		public static void Init(bool force = false)
		{
			if (init && !force)
				return;
			defOut = Console.Out;
			outputStream = new MemoryStream();
			Console.SetOut(log = new Log(defOut));
		}

		public static void Info(string text)
		{
			text = $"Info: {text}\r\n";
			if((int)fileLogLevel >= (int)LogLevel.INFO)
				WriteToStream(text);
			if((int)consoleLogLevel >= (int)LogLevel.INFO)
			{
				Console.ForegroundColor = ConsoleColor.White;
				Console.Write(text);
				Console.ResetColor();
			}
		}

		public static void Warning(string text)
		{
			text = $"Warning: {text}\r\n";
			if((int)fileLogLevel >= (int)LogLevel.WARNING)
				WriteToStream(text);
			if((int)consoleLogLevel >= (int)LogLevel.WARNING)
			{
				Console.ForegroundColor = ConsoleColor.Yellow;
				Console.Write(text);
				Console.ResetColor();
			}
		}

		public static void Error(string text)
		{
			text = $"Error: {text}\r\n";
			if((int)fileLogLevel >= (int)LogLevel.ERROR)
				WriteToStream(text);
			if((int)consoleLogLevel >= (int)LogLevel.ERROR)
			{
				Console.ForegroundColor = ConsoleColor.Red;
				Console.Write(text);
				Console.ResetColor();
			}
		}

		public static void WriteToStream(string s)
		{
			s = DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss") + " [XPF] " + s;
			byte[] b = Encoding.UTF8.GetBytes(s);
			outputStream?.Write(b, 0, b.Length);
		}

		public static void Close()
		{
			outputStream?.Flush();
			outputStream?.Close();
			outputStream = null;
		}

		public static void SetLogFile(string path)
		{
			Stream s;
			try
			{
				s = System.IO.File.OpenWrite(path);
				outputStream.Flush();
				if(outputStream is MemoryStream)
				{
					outputStream.Seek(0, SeekOrigin.Begin);
					outputStream.CopyTo(s);
				}
				outputStream = s;
				Info($"Log output successfully set to '{path}'");
			}
			catch(Exception x)
			{
				Error($"Couldn't open file {path} as log file");
				Error(x.ToString());
			}
		}
	}

	class Log : TextWriter
	{
		private TextWriter defOut;
		private static readonly Regex rInfo = new Regex("^(Info:( )*)+(.*)$", RegexOptions.Compiled);
		private static readonly Regex rWarning = new Regex("^(Warning:( )*)+(.*)$", RegexOptions.Compiled);
		private static readonly Regex rError = new Regex("^(Error:( )*)+(.*)$", RegexOptions.Compiled);

		public Log(TextWriter defOut)
		{
			this.defOut = defOut;
		}

		public override void WriteLine(string value)
		{
			if (value.StartsWith("Info:"))
				Logger.Info(rInfo.Replace(value, "$3"));
			else if (value.StartsWith("Warning:"))
				Logger.Warning(rWarning.Replace(value, "$3"));
			else if (value.StartsWith("Error:"))
				Logger.Error(rError.Replace(value, "$3"));
			else
				this.defOut.WriteLine(value);
		}


		public override Encoding Encoding => this.defOut.Encoding;
		public override IFormatProvider FormatProvider => this.defOut.FormatProvider;

		public override string NewLine
		{
			get
			{
				return this.defOut.NewLine;
			}
			set
			{
				this.defOut.NewLine = value;
			}
		}

		public override void Close()
		{
			this.defOut.Close();
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing)
				((IDisposable)this.defOut).Dispose();
		}

		public override void Flush()
		{
			this.defOut.Flush();
		}

		public override void Write(char value)
		{
			this.defOut.Write(value);
		}

		public override void Write(char[] buffer)
		{
			this.defOut.Write(buffer);
		}

		public override void Write(char[] buffer, int index, int count)
		{
			this.defOut.Write(buffer, index, count);
		}

		public override void Write(bool value)
		{
			this.defOut.Write(value);
		}

		public override void Write(int value)
		{
			this.defOut.Write(value);
		}

		public override void Write(uint value)
		{
			this.defOut.Write(value);
		}

		public override void Write(long value)
		{
			this.defOut.Write(value);
		}

		public override void Write(ulong value)
		{
			this.defOut.Write(value);
		}

		public override void Write(float value)
		{
			this.defOut.Write(value);
		}

		public override void Write(double value)
		{
			this.defOut.Write(value);
		}

		public override void Write(Decimal value)
		{
			this.defOut.Write(value);
		}

		public override void Write(string value)
		{
			this.defOut.Write(value);
		}

		public override void Write(object value)
		{
			this.defOut.Write(value);
		}

		public override void Write(string format, object arg0)
		{
			this.defOut.Write(format, arg0);
		}

		public override void Write(string format, object arg0, object arg1)
		{
			this.defOut.Write(format, arg0, arg1);
		}

		public override void Write(string format, object arg0, object arg1, object arg2)
		{
			this.defOut.Write(format, arg0, arg1, arg2);
		}

		public override void Write(string format, object[] arg)
		{
			this.defOut.Write(format, arg);
		}

		public override void WriteLine()
		{
			this.defOut.WriteLine();
		}

		public override void WriteLine(char value)
		{
			this.defOut.WriteLine(value);
		}

		public override void WriteLine(decimal value)
		{
			this.defOut.WriteLine(value);
		}

		public override void WriteLine(char[] buffer)
		{
			this.defOut.WriteLine(buffer);
		}

		public override void WriteLine(char[] buffer, int index, int count)
		{
			this.defOut.WriteLine(buffer, index, count);
		}

		public override void WriteLine(bool value)
		{
			this.defOut.WriteLine(value);
		}

		public override void WriteLine(int value)
		{
			this.defOut.WriteLine(value);
		}

		public override void WriteLine(uint value)
		{
			this.defOut.WriteLine(value);
		}

		public override void WriteLine(long value)
		{
			this.defOut.WriteLine(value);
		}

		public override void WriteLine(ulong value)
		{
			this.defOut.WriteLine(value);
		}

		public override void WriteLine(float value)
		{
			this.defOut.WriteLine(value);
		}

		public override void WriteLine(double value)
		{
			this.defOut.WriteLine(value);
		}

		public override void WriteLine(object value)
		{
			this.defOut.WriteLine(value);
		}

		public override void WriteLine(string format, object arg0)
		{
			this.defOut.WriteLine(format, arg0);
		}

		public override void WriteLine(string format, object arg0, object arg1)
		{
			this.defOut.WriteLine(format, arg0, arg1);
		}

		public override void WriteLine(string format, object arg0, object arg1, object arg2)
		{
			this.defOut.WriteLine(format, arg0, arg1, arg2);
		}

		public override void WriteLine(string format, object[] arg)
		{
			this.defOut.WriteLine(format, arg);
		}
	}
}
