using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XMLObjects;

namespace XPF
{
	public class Action : ActionBase
	{
		public List<IAction> actions = new List<IAction>();

		public Action(string id = null) : base(id)
		{ }
		public Action(XMLNode node, Managers m) : base(node)
		{
			if (node.HasValue)
				throw new Exception("ACTION mustn't have text");
			foreach (XMLNode subNode in node.SubNodes)
				this.actions.Add(m.am.GetObject(subNode));
		}

		public override void Run(File f)
		{
			try
			{
				foreach (IAction action in this.actions)
					action.Run(f);
			}
			catch (ReturnAction)
			{
				return;
			}
		}

		[Register]
		public static void Register(Managers m)
		{
			m.am.Register("action", (node, managers) => new Action(node, managers));
		}
	}

	public class ReturnAction : Exception, IAction
	{
		public ReturnAction(string id = null) : base(id)
		{ }
        public ReturnAction(XMLNode node)
		{
			if (node.Attributes.Count != 0)
				throw new Exception("RETURN is a special action which doesn't accept any attributes");
		}

		public virtual void Run(File f)
		{
			throw this;
		}

		[Register]
		public static void Register(Managers m)
		{
			m.am.Register("return", (node, managers) => new ReturnAction(node));
		}
	}

	public class EmptyAction : IAction
	{
		public void Run(File f)
		{ }
	}

	public class CondAction : ActionBase
	{
		public ICondition cond;

		public CondAction(string id = null) : base(id)
		{ }
		public CondAction(XMLNode node, Managers m) : base(node)
		{
			this.cond = new AndCondition(node, m);
		}

		public override void Run(File f)
		{
			cond.Test(f);
		}

		[Register]
		public void Register(Managers m)
		{
			Manager<IAction>.ObjectCreator oc = (node, managers) => new CondAction(node, managers);
			m.am.Register("cond", oc);
			m.am.Register("redo", oc);
		}
	}
}
