using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace XPF
{
	public static class Base
	{
		public static void Extract()
		{
			Console.WriteLine("Info: Extracting base.xml to ./base.xml");
			using(Stream stream = Assembly.GetExecutingAssembly().GetManifestResourceStream("XPF.base.xml"))
			using(StreamReader reader = new StreamReader(stream))
				System.IO.File.WriteAllText("./base.xml", reader.ReadToEnd());
			Console.WriteLine("Info: base.xml successfully extracted");
		}
	}
}
