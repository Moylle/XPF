using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using XMLObjects;

namespace XPF
{
	public class ClassInclude : IInclude
	{
		public string name;
		public Managers m;

		public ClassInclude()
		{ }

		public ClassInclude(XMLNode node, Managers m)
		{
			if (node == null)
				throw new ArgumentNullException(nameof(node));
			this.m = m;
			if (!node.HasValue)
				throw new Exception("CLASS must contain text");
			this.name = node.Value;
		}

		public virtual void Include()
		{
			Console.WriteLine($"Info: Including class '{this.name}'");
			ParameterInfo[] paramters;
			bool methodFound = false;
			bool classFound = false;
			Type t;
			foreach(Assembly a in AppDomain.CurrentDomain.GetAssemblies())
			{
				t = a.GetType(this.name);
				if(t == null)
					continue;
				classFound = true;
				foreach(MethodInfo method in t.GetMethods())
					if(method.GetCustomAttribute<RegisterAttribute>() != null && method.IsStatic && (paramters = method.GetParameters()).Length == 1 && paramters[0].ParameterType == typeof(Managers))
					{
						Console.WriteLine("Info: Found Register-Method. Invoking...");
						method.Invoke(null, new object[] { this.m });
						methodFound = true;
					}
				break;
			}
			if(classFound && methodFound)
				Console.WriteLine($"Info: Class '{this.name}' successfully included");
			else if(classFound)
				Console.WriteLine("Warning: No suitable Register-method found");
			else
				Console.WriteLine($"Error: No class '{this.name}' found");
		}

		[Register]
		public static void Register(Managers m)
		{
			m.im.Register("class", (node, managers) => new ClassInclude(node, managers));
		}
	}

	public class XMLInclude : IInclude
	{
		public string path;
		public Managers m;

		public XMLInclude()
		{ }
		public XMLInclude(XMLNode node, Managers m)
		{
			if (node == null)
				throw new ArgumentNullException(nameof(node));
			this.m = m;
			if (!node.HasValue)
				throw new Exception("XML must contain text");
			this.path = node.Value;
		}

		public virtual void Include()
		{
			Console.WriteLine("Info: Including external XML file '" + this.path + "'");
			if (!System.IO.File.Exists(path))
				throw new Exception("File doesn't exist!");
			Console.WriteLine("Info: Parsing XML");
			XMLNode node = XMLNode.CreateByPath(this.path);
			this.m.xm.rootNode.SubNodes.AddRange(node.SubNodes);
			Console.WriteLine("Info: External XML successfully included");
		}

		[Register]
		public static void Register(Managers m)
		{
			m.im.Register("xml", (node, managers) => new XMLInclude(node, managers));
			m.im.Register("base", (node, managers) =>
			{
				XMLInclude xmli = new XMLInclude();
				xmli.m = managers;
				xmli.path = "./base.xml";
				return xmli;
			});
		}
	}
	
	public class AssemblyInclude : IInclude
	{
		public Managers m;
		public string path;

		public AssemblyInclude()
		{ }
		public AssemblyInclude(XMLNode node, Managers m)
		{
			if (node == null)
				throw new ArgumentNullException(nameof(node));
			this.m = m;
			if (!node.HasValue)
				throw new Exception("ASSEMBLY must contain text");
			this.path = node.Value;
		}

		public virtual void Include()
		{
			Console.WriteLine($"Info: Including external assembly '{this.path}'");
			if(!System.IO.File.Exists(path))
				throw new Exception("File doesn't exist!");
			Assembly a = Assembly.LoadFrom(this.path);
			AppDomain.CurrentDomain.Load(a.FullName);
			Console.WriteLine("Info: External assembly successfully loaded");
		}

		[Register]
		public static void Register(Managers m)
		{
			IncludeManager.ObjectCreator oc = (node, managers) => new AssemblyInclude(node, managers);
			m.im.Register("assembly", oc);
			m.im.Register("dll", oc);
			m.im.Register("exe", oc);
		}
	}

	public class InternalXMLInclude : IInclude
	{
		public Managers m;
		public string val;

		public InternalXMLInclude()
		{ }
		public InternalXMLInclude(XMLNode node, Managers m)
		{
			if (node == null)
				throw new ArgumentNullException(nameof(node));
			this.m = m;
			if (!node.HasValue)
				throw new Exception("INTERNAL must contain text");
			this.val = node.Value;
		}

		public virtual void Include()
		{
			try
			{
				Console.WriteLine($"Info: Trying to parse and load XMLNode from '{this.val}'");
				string clazz = this.val.Substring(0, this.val.LastIndexOf('.'));
				string s = this.val.Substring(this.val.LastIndexOf('.') + 1);
				Type t = Type.GetType(clazz, true);
				MethodInfo meth;
				FieldInfo field;
				PropertyInfo prop;
				XMLNode node;
				if(s.EndsWith("()"))
				{
					s = s.Substring(0, s.Length - 2);
					meth = t.GetMethod(s, BindingFlags.Static);
					node = (XMLNode)meth.Invoke(null, new object[0]);
				}
				else
					try
					{
						field = t.GetField(s, BindingFlags.Static);
						node = (XMLNode)field.GetValue(null);
					}
					catch
					{
						prop = t.GetProperty(s, BindingFlags.Static);
						node = (XMLNode)prop.GetValue(null);
					}
				this.m.xm.rootNode.SubNodes.AddRange(node.SubNodes);
				Console.WriteLine("Info: XMLNode successfully loaded and included");
			}
			catch
			{
				throw new Exception($"Error while parsing or loading XMLNode from '{this.val} '");
			}
		}

		[Register]
		public static void Register(Managers m)
		{
			m.im.Register("internalxml", (node, managers) => new InternalXMLInclude(node, managers));
		}
	}
}
