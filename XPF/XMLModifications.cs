using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XMLObjects;

namespace XPF
{
	public static class XMLModifications
	{
		public static void Apply()
		{
			XMLNode.ValueAccess += OnValueRequest;
			XMLNode.SubNodesAccess += OnSubNodesRequest;
			XMLNode.HasValueAccess += OnHasValueRequest;
			XMLNode.HasSubNodesAccess += OnHasSubNodesRequest;
		}

		static void OnValueRequest(EventArgs<string> e)
		{
			if(e.accessType == AccessType.GET && e.node.HasAttr("empty") && e.node["empty"] == "true")
				e.value = "";
		}
		static void OnSubNodesRequest(EventArgs<List<XMLNode>> e)
		{
			if(e.accessType == AccessType.GET && e.node.HasAttr("empty") && e.node["empty"] == "true")
				e.value = new List<XMLNode>();
		}
		static void OnHasValueRequest(EventArgs<bool> e)
		{
			if(e.node.HasAttr("empty") && e.node["empty"] == "true")
				e.value = true;
		}
		static void OnHasSubNodesRequest(EventArgs<bool> e)
		{
			if(e.node.HasAttr("empty") && e.node["empty"] == "true")
				e.value = false;
		}
	}
}
