using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XMLObjects;

namespace XPF
{
	public interface IDynamicString
	{
		string GetString();
	}

	public abstract class DynamicStringBase : IDynamicString
	{
		public readonly string id;

		public DynamicStringBase(string id = null)
		{
			this.id = id;
		}

		public DynamicStringBase(XMLNode node)
		{
			if(node == null)
				throw new ArgumentNullException(nameof(node));
			if(node.HasAttr("id"))
				this.id = node.Attributes["id"];
		}

		public abstract string GetString();
	}
}
