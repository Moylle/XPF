using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XMLObjects;

namespace XPF
{
	public class IncludeManager : Manager<IInclude>
	{
		public IncludeManager(Managers m) : base(m)
		{
			this.unresolvedCreator = (id) => { throw new Exception($"Couldn't resolve Include with id '{id}'. Includes are resolved immediately"); };
		}

		public void InitBasicIncludes()
		{
			Include.Register(this.Managers);
			ClassInclude.Register(this.Managers);
			XMLInclude.Register(this.Managers);
			AssemblyInclude.Register(this.Managers);
			InternalXMLInclude.Register(this.Managers);
		}
	}

	public class ConditionManager : Manager<ICondition>
	{
		public ConditionManager(Managers m) : base(m)
		{
			ConditionManager cm = this;
			this.unresolvedCreator = (id) => new UnresolvedCondition(id, cm);
		}
	}

	public class ActionManager : Manager<IAction>
	{
		public ActionManager(Managers m) : base(m)
		{
			ActionManager am = this;
			this.unresolvedCreator = (id) => new UnresolvedAction(id, am);
		}
	}

	public class OnManager : Manager<IOn>
	{
		public OnManager(Managers m) : base(m)
		{
			this.unresolvedCreator = (id) => { throw new Exception($"Couldn't resolve On with id '{id}'. Ons are resolved immediately"); };
		}
	}

	public class SettingsManager : Manager<ISetting>
	{
		public SettingsManager(Managers m) : base(m)
		{
			this.unresolvedCreator = (id) => { throw new Exception($"Couldn't resolve Setting with id '{id}'. Settings are resolved immediately"); };
		}
	}

	public class Managers
	{
		public ActionManager am
		{
			get;
			private set;
		}
		public ConditionManager cm
		{
			get;
			private set;
		}
		public OnManager om
		{
			get;
			private set;
		}
		public IncludeManager im
		{
			get;
			private set;
		}
		public SettingsManager sm
		{
			get;
			private set;

		}
		public DynamicStringManager dm
		{
			get;
			private set;
		}
		public XMLManager xm
		{
			get;
			private set;
		}

		public Managers(ActionManager am, ConditionManager cm, OnManager om, IncludeManager im, SettingsManager sm, DynamicStringManager dm, XMLManager xm)
		{
			this.am = am;
			this.cm = cm;
			this.om = om;
			this.im = im;
			this.sm = sm;
			this.dm = dm;
			this.xm = xm;
		}

		private Managers()
		{ }


		public static Managers Init()
		{
			Managers m = new Managers();
			m.am = new ActionManager(m);
			m.cm = new ConditionManager(m);
			m.om = new OnManager(m);
			m.im = new IncludeManager(m);
			m.sm = new SettingsManager(m);
			m.dm = new DynamicStringManager(m);
			m.xm = new XMLManager(m);
			
			m.im.InitBasicIncludes();
			return m;
		}
	}
}
