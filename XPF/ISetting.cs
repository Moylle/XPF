﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XMLObjects;

namespace XPF
{
	public interface ISetting
	{
		void Apply();
	}

	public abstract class SettingBase : ISetting
	{
		public readonly string id;

		public SettingBase(string id = null)
		{
			this.id = id;
		}

		public SettingBase(XMLNode node)
		{
			if (node.HasAttr("id"))
				this.id = node.Attributes["id"];
		}

		public abstract void Apply();
	}
}
