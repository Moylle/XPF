using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace XPF
{
	public class File
	{
		private static string srcRootPath;
		private static string dstRootPath;
		private static Encoding encoding = Encoding.ASCII;

		public static string RootPath
		{
			get
			{
				return srcRootPath;
			}
			set
			{
				srcRootPath = Regex.Replace(value, @"^(.*?)(\\|/)?$", "$1");
			}
		}
		public static string DstRootPath
		{
			get
			{
				return dstRootPath;
			}
			set
			{
				dstRootPath = Regex.Replace(value, @"^(.*?)(\\|/)?$", "$1");
			}
		}

		public static Encoding Encoding
		{
			get
			{
				return encoding;
			}
			set
			{
				encoding = value ?? Encoding.ASCII;
			}
		}


		public string TextContent
		{
			get
			{
				return this.textContent ?? (this.textContent = ReadText(encoding));
			}
			set
			{
				this.textContent = value;
				this.textContentChanged = true;
			}
		}
		public byte[] ByteContent
		{
			get
			{
				return this.byteContent ?? (this.byteContent = ReadBinary());
			}
			set
			{
				this.byteContent = value;
				this.byteContentChanged = true;
			}
		}

		private string textContent;
		private byte[] byteContent;

		public bool textContentChanged = false;
		public bool byteContentChanged = false;

		public readonly string absolutePath;

		public string relativeDir;
		protected string name;
		protected string newName;
		public string Name
		{
			get
			{
				return this.name;
			}
			set
			{
				this.newName = value;
			}
		}
		public string NewName
		{
			get
			{
				return this.newName ?? this.name;
			}
			set
			{
				this.newName = value;
			}
		}
		public string RelativePath
		{
			get
			{
				return Path.Combine(this.relativeDir, this.Name);
			}
		}
		public string NewRelativePath
		{
			get
			{
				return Path.Combine(this.relativeDir, this.NewName);
			}
		}

		public string Extension
		{
			get
			{
				return Path.GetExtension(this.Name);
			}
		}

		public bool HasTextContent
		{
			get
			{
				return this.textContent != null;
			}
		}
		public bool HasBinaryContent
		{
			get
			{
				return this.byteContent != null;
			}
		}

		public string DestinationPath
		{
			get
			{
				return Path.Combine(dstRootPath, this.NewRelativePath);
			}
		}

		public readonly bool isDirectory;

		public Dictionary<string, object> storedObjects = new Dictionary<string, object>();
		public Dictionary<string, object> storedClassObjects = new Dictionary<string, object>();

		public File(string absolutePath, bool isDirectory = false)
		{
			this.absolutePath = absolutePath;
			this.isDirectory = isDirectory;
			this.relativeDir = (Path.GetDirectoryName(absolutePath) + "\\").Substring(srcRootPath.Length + 1);
			this.name = Path.GetFileName(absolutePath);
		}

		public string ReadText(Encoding e)
		{
			if (this.isDirectory)
				throw new Exception("Can't read text of a directory");
			return this.textContent = System.IO.File.ReadAllText(this.absolutePath, e);
		}
		public byte[] ReadBinary()
		{
			if (this.isDirectory)
				throw new Exception("Can't read bytes of a directory");
			return this.byteContent = System.IO.File.ReadAllBytes(this.absolutePath);
		}

		
		public Dictionary<string, object> GetStoredObjects(string[] ids)
		{
			Dictionary<string, object> objects = new Dictionary<string, object>();
			bool idObj;
			bool classObj;

			foreach(string id in ids)
			{
				if(idObj = this.storedObjects.ContainsKey(id))
					objects.Add(id, this.storedObjects[id]);
				if(classObj = this.storedClassObjects.ContainsKey(id))
					if(idObj)
						Console.WriteLine($"Warning: Key '{id}' is used as id and as class. Will take object bound to id");
					else
						objects.Add(id, this.storedClassObjects[id]);
				if(!idObj && !classObj)
					Console.WriteLine($"Warning: Key '{id} ' not found");
			}
			
			return objects;
		}
	}
}
