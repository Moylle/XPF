using System;

namespace XPF
{
	public class Unresolved<T>
	{
		public readonly string id;
		public T resolved;
		public bool IsResolved
		{
			get;
			private set;
		}
		public T Resolved
		{
			get
			{
				if(!this.IsResolved)
				{
					this.resolved = Resolve();
					this.IsResolved = true;
				}
				return this.resolved;
			}
		}
		public Manager<T> manager;

		public Unresolved(string id, Manager<T> manager)
		{
			this.id = id;
			this.manager = manager;
		}
		
		public T Resolve()
		{
			return this.manager.ResolveObject(this);
		}
	}

	public class UnresolvedCondition : ICondition
	{
		private Unresolved<ICondition> unresolved;

		public UnresolvedCondition(string id, ConditionManager cm)
		{
			this.unresolved = new Unresolved<ICondition>(id, cm);
		}

		public bool Test(File f)
		{
			return this.unresolved.Resolved.Test(f);
		}

		public object GetReturnValue()
		{
			return this.unresolved.Resolved.GetReturnValue();
		}
	}

	public class UnresolvedAction : IAction
	{
		private Unresolved<IAction> unresolved;

		public UnresolvedAction(string id, ActionManager am)
		{
			this.unresolved = new Unresolved<IAction>(id, am);
		}

		public void Run(File f)
		{
			this.unresolved.Resolved.Run(f);
		}
	}

	public class UnresolvedDynamicString : IDynamicString
	{
		private Unresolved<IDynamicString> unresolved;

		public UnresolvedDynamicString(string id, DynamicStringManager dm)
		{
			this.unresolved = new Unresolved<IDynamicString>(id, dm);
		}

		public string GetString()
		{
			return this.unresolved.Resolved.GetString();
		}
	}
}
